<?php
declare(strict_types=1);
/**
 * CustomPriceFix
 *
 * @author  Andrey Rapshtynsky <andrey.bh@gmail.com>
 */
namespace Rapa\CustomPriceFix\Plugin\Tax\Model\Sales\Total\Quote;

use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Store\Model\Store;
use Magento\Tax\Api\Data\TaxDetailsItemInterface;
use Magento\Tax\Helper\Data as TaxHelper;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;

class CommonTaxCollectorPlugin
{
    /**
     * @var TaxHelper
     */
    protected $_taxHelper;

    /**
     * CommonTaxCollectorPlugin constructor.
     *
     * @param TaxHelper $taxHelper
     */
    public function __construct(
        TaxHelper $taxHelper
    ) {
        $this->_taxHelper = $taxHelper;
    }

    /**
     * @param CommonTaxCollector $subject
     * @param CommonTaxCollector $result
     * @param AbstractItem $quoteItem
     * @param TaxDetailsItemInterface $itemTaxDetails
     * @param TaxDetailsItemInterface $baseItemTaxDetails
     * @param Store $store
     * @return CommonTaxCollector
     */
    public function afterUpdateItemTaxInfo (
        CommonTaxCollector $subject,
        CommonTaxCollector $result,
        AbstractItem $quoteItem,
        TaxDetailsItemInterface $itemTaxDetails,
        TaxDetailsItemInterface $baseItemTaxDetails,
        Store $store
    ) {
        if ($quoteItem->getCustomPrice() && $this->_taxHelper->applyTaxOnCustomPrice()) {
            $quoteItem->setCustomPrice($itemTaxDetails->getPrice());
        }

        return $result;
    }
}