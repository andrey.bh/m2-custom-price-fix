<?php
declare(strict_types=1);
/**
 * CustomPriceFix
 *
 * @author  Andrey Rapshtynsky <andrey.bh@gmail.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Rapa_CustomPriceFix',
    __DIR__
);